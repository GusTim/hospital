package sample.list_patient;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import sample.change_patient.Controller_c;

import java.io.IOException;
import java.sql.*;

public class Controller {

    private ObservableList<Patient> usersData = FXCollections.observableArrayList();


    @FXML
    private TableView<Patient> tableUsers;

    @FXML
    private TableColumn<Patient, Integer> idColumn;

    @FXML
    private TableColumn<Patient, String> FirstNameColumn;

    @FXML
    private TableColumn<Patient, String> LastNameColumn;

    @FXML
    private TableColumn<Patient, String> PatherNameColumn;

    @FXML
    private TableColumn<Patient, Integer> DiagnosisColumn;

    @FXML
    private TableColumn<Patient, Integer> WardsColumn;

    @FXML
    private Button btn_delete_patient;

    @FXML
    private Button btn_add_patient;

    @FXML
    private Button btn_refresh_table_people;

    @FXML
    private Button btn_change_patient;

    @FXML
    private Button btn_spec_host;

    @FXML
    private Button btn_table_wards;

    // инициализируем форму данными
    @FXML
    private void initialize() {
        initData();

        btn_delete_patient.setOnAction(event -> {
            delete_row_people_table();
        });

        btn_add_patient.setOnAction(event -> {
            add_patient_people_table();
        });

        btn_refresh_table_people.setOnAction(event -> {
            update_people_table();
        });

        btn_change_patient.setOnAction(event -> {
            change_patient();
        });

        btn_spec_host.setOnAction(event -> {
            spec_host();
        });

        btn_table_wards.setOnAction(event -> {
            table_wards();
        });

        // устанавливаем тип и значение которое должно хранится в колонке
        idColumn.setCellValueFactory(new PropertyValueFactory<Patient, Integer>("id"));
        FirstNameColumn.setCellValueFactory(new PropertyValueFactory<Patient, String>("first_name"));
        LastNameColumn.setCellValueFactory(new PropertyValueFactory<Patient, String>("last_name"));
        PatherNameColumn.setCellValueFactory(new PropertyValueFactory<Patient, String>("pather_name"));
        DiagnosisColumn.setCellValueFactory(new PropertyValueFactory<Patient, Integer>("diagnosis_id"));
        WardsColumn.setCellValueFactory(new PropertyValueFactory<Patient, Integer>("wards_id"));

        // заполняем таблицу данными
        tableUsers.setItems(usersData);
    }

    // подготавливаем данные для таблицы
    // вы можете получать их с базы данных
    private void initData() {
        update_people_table();
    }




    public void update_people_table()
    {
        tableUsers.getItems().clear();

        String selectTableSQL = "SELECT * from \"people\"";

        try {
            Connection dbConnection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","C##GT98","1234");
            Statement statement = dbConnection.createStatement();

            // выбираем данные с БД
            ResultSet rs = statement.executeQuery(selectTableSQL);

            // И если что то было получено то цикл while сработает
            while (rs.next()) {
                int id = rs.getInt("id");
                String first_name = rs.getString("first_name");
                String last_name = rs.getString("last_name");
                String pather_name = rs.getString("pather_name");
                int diagnosis_id = rs.getInt("diagnosis_id");
                int wards_id = rs.getInt("ward_id");


                System.out.println("Patient : " + id +" "+first_name+" "+last_name+" "+pather_name+" "+diagnosis_id+" "+wards_id);


                usersData.add(new Patient(id, first_name, last_name, pather_name, diagnosis_id, wards_id));

            }
        } catch (SQLException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Ошибка");
            alert.setHeaderText("Произошла ошибка при обновление!");
            alert.setContentText(e.getMessage());
            alert.showAndWait();
            System.out.println(e.getMessage());
        }
    }

    public void delete_row_people_table()
    {


        Patient person = tableUsers.getSelectionModel().getSelectedItem();
        System.out.println(person.getId());

        int row_id =  person.getId();

        String deleteTableSQL = "DELETE \"people\" where \"people\".\"id\" = "+row_id;

        try {
            Connection dbConnection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","C##GT98","1234");
            Statement statement = dbConnection.createStatement();

            // выполняем запрос delete SQL
            statement.execute(deleteTableSQL);
            System.out.println("Record is deleted from table!");
            update_people_table();
        } catch (SQLException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Ошибка");
            alert.setHeaderText("Произошла ошибка при удаление!");
            alert.setContentText(e.getMessage());
            alert.showAndWait();
            System.out.println(e.getMessage());
        }


    }

    public void add_patient_people_table()
    {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/sample/add_patient/add.fxml"));

        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.showAndWait();
    }

    public void change_patient() {
        Patient person = tableUsers.getSelectionModel().getSelectedItem();

        int row_id =  person.getId();


        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/sample/change_patient/change.fxml"));

        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Controller_c mainController = loader.<Controller_c>getController();
        mainController.setUsername(row_id);

        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));

        stage.showAndWait();
    }

    public void spec_host(){

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/sample/spec_hos/spec_hoc.fxml"));

        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.showAndWait();
        Stage stage_1 = (Stage) btn_add_patient.getScene().getWindow();
        // do what you have to do
        stage_1.close();

    }

    public void table_wards(){

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/sample/wards/sample.fxml"));

        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.showAndWait();
        Stage stage_1 = (Stage) btn_add_patient.getScene().getWindow();
        // do what you have to do
        stage_1.close();

    }
}

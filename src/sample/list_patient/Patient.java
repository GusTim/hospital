package sample.list_patient;

public class Patient {
    private int id;
    private String first_name;
    private String last_name;
    private String pather_name;
    private int diagnosis_id;
    private int wards_id;


    public Patient(int id, String first_name, String last_name, String pather_name, int diagnosis_id, int wards_id)
    {
        this.id = id;
        this.first_name = first_name;
        this.last_name = last_name;
        this.pather_name = pather_name;
        this.diagnosis_id = diagnosis_id;
        this.wards_id = wards_id;

    }

    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }
    public String getFirst_name() {
        return first_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }
    public String getLast_name() {
        return last_name;
    }

    public void setPather_name(String pather_name) {
        this.pather_name = pather_name;
    }
    public String getPather_name() {
        return pather_name;
    }

    public void setDiagnosis_id(int diagnosis_id) {
        this.diagnosis_id = diagnosis_id;
    }
    public int getDiagnosis_id() {
        return diagnosis_id;
    }

    public void setWards_id(int wards_id) {
        this.wards_id = wards_id;
    }
    public int getWards_id() {
        return wards_id;
    }

}




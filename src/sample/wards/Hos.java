package sample.wards;

public class Hos {
    private int id;
    private int w1;
    private int w2;
    private int w3;
    private int w4;


    public Hos(int id, int w1,int w2, int w3, int w4)
    {
        this.id = id;
        this.w1 = w1;
        this.w2 = w2;
        this.w3 = w3;
        this.w4 = w4;


    }

    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }

    public void setW1(int w1) {
        this.w1 = w1;
    }
    public int getW1() {
        return w1;
    }

    public void setW2(int w2) {
        this.w2 = w2;
    }
    public int getW2() {
        return w2;
    }

    public void setW3(int w3) {
        this.w3 = w3;
    }
    public int getW3() {
        return w3;
    }

    public void setW4(int w4) {
        this.w4 = w4;
    }
    public int getW4() {
        return w4;
    }


}




package sample.wards;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.*;

public class Controller {

    private ObservableList<Hos> WardD = FXCollections.observableArrayList();


    @FXML
    private TableView<Hos> tableWardCount;

    @FXML
    private TableColumn<Hos, Integer> idColumn;

    @FXML
    private TableColumn<Hos, Integer> NameColumn;

    @FXML
    private TableColumn<Hos, Integer> WArdMaxCountColumn;

    @FXML
    private TableColumn<Hos, Integer> WardCountColumn;


    @FXML
    private TableColumn<Hos, Integer> DiagnosisInWardColumn;


    @FXML
    private Button btn_spec_host;

    @FXML
    private Button btn_patient;


    int id = 0;



    // инициализируем форму данными
    @FXML
    private void initialize() {
        initData();


        btn_spec_host.setOnAction(event -> {
            spec_host();
        });

        btn_patient.setOnAction(event -> {
            patient();
        });


        // устанавливаем тип и значение которое должно хранится в колонке
        idColumn.setCellValueFactory(new PropertyValueFactory<Hos, Integer>("id"));
        NameColumn.setCellValueFactory(new PropertyValueFactory<Hos, Integer>("w1"));
        WArdMaxCountColumn.setCellValueFactory(new PropertyValueFactory<Hos, Integer>("w2"));
        WardCountColumn.setCellValueFactory(new PropertyValueFactory<Hos, Integer>("w3"));
        DiagnosisInWardColumn.setCellValueFactory(new PropertyValueFactory<Hos, Integer>("w4"));


        // заполняем таблицу данными
        tableWardCount.setItems(WardD);
    }

    // подготавливаем данные для таблицы
    // вы можете получать их с базы данных
    private void initData() {
        update_people_table();
    }




    public void update_people_table()
    {
        tableWardCount.getItems().clear();

        String selectTableSQL = "SELECT * from \"REPRESENTATION_4\"";

        try {
            Connection dbConnection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","C##GT98","1234");
            Statement statement = dbConnection.createStatement();

            // выбираем данные с БД
            ResultSet rs = statement.executeQuery(selectTableSQL);

            // И если что то было получено то цикл while сработает
            while (rs.next()) {
                id++;
                int wards = rs.getInt("wards");
                int wcount = rs.getInt("wcount");
                int max =rs.getInt("max");
                int diagnosis_id = rs.getInt("diagnosis_id");

                System.out.println(id+"  " + wards +"  "+ wcount +"  " +max +"  "+diagnosis_id);
                WardD.add(new Hos(id, wards, wcount, max, diagnosis_id));

            }
        } catch (SQLException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Ошибка");
            alert.setHeaderText("Произошла ошибка при обновление!");
            alert.setContentText(e.getMessage());
            alert.showAndWait();
            System.out.println(e.getMessage());
        }
    }


    public void spec_host(){

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/sample/spec_hos/spec_hoc.fxml"));

        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.showAndWait();
        Stage stage_1 = (Stage) btn_patient.getScene().getWindow();
        // do what you have to do
        stage_1.close();

    }

    public void patient(){

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/sample/list_patient/sample.fxml"));

        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.showAndWait();
        Stage stage_1 = (Stage) btn_patient.getScene().getWindow();
        // do what you have to do
        stage_1.close();

    }
}

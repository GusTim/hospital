package sample.change_patient;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import sample.list_patient.Patient;

import java.awt.*;
import java.sql.*;

public class Controller_c {

    int row_id;

    @FXML
    private TextField edt_last_name ;
    @FXML
    private TextField edt_first_name ;
    @FXML
    private TextField edt_pather_name ;
    @FXML
    private TextField edt_diagnosis_id ;
    @FXML
    private TextField edt_ward_id;
    @FXML
    private Button btn_add_new_patient;
    @FXML
    private Button btn_no_add_new_patient;
    @FXML
    private Button btn_refresh_change;

    @FXML
    private void initialize() {

        btn_refresh_change.setOnAction(event -> {
            refresh_new_patient();
        });

        btn_add_new_patient.setOnAction(event -> {
            add_new_patient();
        });
        btn_no_add_new_patient.setOnAction(event -> {
            no_add_new_patient();
        });


        System.out.println(row_id);

    }

    public void setUsername(int row_id) {
        this.row_id = row_id;
    }


    public  void add_new_patient()
    {

        System.out.println(row_id);

        String first_name = edt_first_name.getText();
        String last_name = edt_last_name.getText();
        String pather_name =edt_pather_name.getText();
        int diagnosis_id = Integer.parseInt(edt_diagnosis_id.getText());
        int wards_id = Integer.parseInt(edt_ward_id.getText());



        System.out.println("Patient : " + row_id +" "+first_name+" "+last_name+" "+pather_name+" "+diagnosis_id+" "+wards_id);



        String updateTableSQL= "UPDATE  \"people\" SET \"id\" = '"+row_id+"' ,\"first_name\" = '"+ first_name+"',\"last_name\" = '"+last_name+"',\"pather_name\"= '"+pather_name+"',\"diagnosis_id\"= '"+diagnosis_id+"',\"ward_id\" = '"+wards_id+"' where \"people\".\"id\"= "+row_id;


        try {
            Connection dbConnection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","C##GT98","1234");
            Statement statement = dbConnection.createStatement();
            System.out.println(updateTableSQL);
            statement.execute(updateTableSQL);
        } catch (SQLException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Ошибка");
            alert.setHeaderText("Произошла ошибка при изменение!");
            alert.setContentText(e.getMessage());
            alert.showAndWait();
            System.out.println(e.getMessage());
        }


        exit_list_patient();
    }

    public  void refresh_new_patient()
    {

        System.out.println(row_id);

        String selectTableSQL = "SELECT * from \"people\" where \"people\".\"id\"= "+row_id;


        try {
            Connection dbConnection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","C##GT98","1234");
            Statement statement = dbConnection.createStatement();

            // выбираем данные с БД
            ResultSet rs = statement.executeQuery(selectTableSQL);

            // И если что то было получено то цикл while сработает
            while (rs.next()) {
                int id = rs.getInt("id");
                String first_name = rs.getString("first_name");
                String last_name = rs.getString("last_name");
                String pather_name = rs.getString("pather_name");
                int diagnosis_id = rs.getInt("diagnosis_id");
                int wards_id = rs.getInt("ward_id");

                System.out.println("Patient : " + id +" "+first_name+" "+last_name+" "+pather_name+" "+diagnosis_id+" "+wards_id);

                edt_first_name.setText(first_name);
                edt_last_name.setText(last_name);
                edt_pather_name.setText(pather_name);
                edt_diagnosis_id.setText(String.valueOf(diagnosis_id));
                edt_ward_id.setText(String.valueOf(wards_id));

            }
        } catch (SQLException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Ошибка");
            alert.setHeaderText("Произошла ошибка при обновление!");
            alert.setContentText(e.getMessage());
            alert.showAndWait();
            System.out.println(e.getMessage());
        }


        //exit_list_patient();
    }


    public  void no_add_new_patient()
    {
        exit_list_patient();
    }


    public void exit_list_patient()
    {

            btn_add_new_patient.getScene().getWindow().hide();

    }

}

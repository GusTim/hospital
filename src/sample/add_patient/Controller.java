package sample.add_patient;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Controller {

    @FXML
    private TextField edt_last_name ;
    @FXML
    private TextField edt_first_name ;
    @FXML
    private TextField edt_pather_name ;
    @FXML
    private TextField edt_diagnosis_id ;
    @FXML
    private TextField edt_ward_id;
    @FXML
    private Button btn_add_new_patient;
    @FXML
    private Button btn_no_add_new_patient;

    @FXML
    private void initialize() {

        btn_add_new_patient.setOnAction(event -> {
            add_new_patient();
        });
        btn_no_add_new_patient.setOnAction(event -> {
            no_add_new_patient();
        });
    }

    public  void add_new_patient()
    {

        String first_name = edt_first_name.getText();
        String last_name = edt_last_name.getText();
        String pather_name = edt_pather_name.getText();
        int diagnosis_id = Integer.parseInt(edt_diagnosis_id.getText());
        int ward_id = Integer.parseInt(edt_ward_id.getText());


        /*String insertTableSQL = "INSERT INTO \"people\" (\"first_name\",\"last_name\",\"pather_name\",\"diagnosis_id\",\"ward_id\") " +
                "VALUES ('"+first_name+"','"+last_name+"','"+pather_name+","+diagnosis_id+"','"+ward_id+"')";*/
        String insertTableSQL = "INSERT INTO \"people\" (\"first_name\",\"last_name\",\"pather_name\",\"diagnosis_id\",\"ward_id\") " +
                "VALUES ('"+first_name+"','"+last_name+"','"+pather_name+"',"+diagnosis_id+","+ward_id+")";

        try {
            Connection dbConnection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","C##GT98","1234");
            Statement statement = dbConnection.createStatement();

            // выполняем запрос delete SQL
            statement.execute(insertTableSQL);

        } catch (SQLException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Ошибка");
            alert.setHeaderText("Произошла ошибка при добавление!");
            alert.setContentText(e.getMessage());
            alert.showAndWait();
            System.out.println(e.getMessage());
        }


        exit_list_patient();
    }
    public  void no_add_new_patient()
    {
        exit_list_patient();
    }


    public void exit_list_patient()
    {

        btn_add_new_patient.getScene().getWindow().hide();

    }

}

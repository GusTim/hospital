package sample.spec_hos.add_ward;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.awt.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Controller {

    @FXML
    private TextField edt_ward_name ;
    @FXML
    private TextField edt_ward_count ;
    @FXML
    private TextField edt_ward_id ;

    @FXML
    private Button btn_add;
    @FXML
    private Button btn_back;

    public Controller() {
    }

    @FXML
    private void initialize() {

        btn_add.setOnAction(event -> {
            add_new_ward();
        });
        btn_back.setOnAction(event -> {
            no_add_new_ward();
        });
    }

    public  void add_new_ward()
    {
        int ward_id = Integer.parseInt(edt_ward_id.getText());
        String name = edt_ward_name.getText();
        int ward_count = Integer.parseInt(edt_ward_count.getText());

        String insertTableSQL = "INSERT INTO \"wards\" (\"id\",\"name\",\"max_count\") VALUES ("+ward_id+",'"+name+"',"+ward_count+")";

        try {
            Connection dbConnection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","C##GT98","1234");
            Statement statement = dbConnection.createStatement();

            // выполняем запрос delete SQL
            statement.execute(insertTableSQL);

        } catch (SQLException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Ошибка");
            alert.setHeaderText("Произошла ошибка при добавление!");
            alert.setContentText(e.getMessage());
            alert.showAndWait();
            System.out.println(e.getMessage());
        }


        exit_list_patient();
    }
    public  void no_add_new_ward()
    {
        exit_list_patient();
    }


    public void exit_list_patient()
    {

        btn_back.getScene().getWindow().hide();

    }

}

package sample.spec_hos.add_diagnosis;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Controller {

    @FXML
    private TextField edt_diagnosis_name ;

    @FXML
    private TextField edt_diagnosis_id ;

    @FXML
    private Button btn_add;
    @FXML
    private Button btn_back;

    @FXML
    private void initialize() {

        btn_add.setOnAction(event -> {
            add_new_ward();
        });
        btn_back.setOnAction(event -> {
            no_add_new_ward();
        });
    }

    public  void add_new_ward()
    {
        int ward_id = Integer.parseInt(edt_diagnosis_id.getText());
        String name = edt_diagnosis_name.getText();


        String insertTableSQL = "INSERT INTO \"diagnosis\" (\"id\",\"name\") VALUES ("+ward_id+",'"+name+"')";

        try {
            Connection dbConnection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","C##GT98","1234");
            Statement statement = dbConnection.createStatement();

            // выполняем запрос delete SQL
            statement.execute(insertTableSQL);

        } catch (SQLException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Ошибка");
            alert.setHeaderText("Произошла ошибка при добавление!");
            alert.setContentText(e.getMessage());
            alert.showAndWait();
            System.out.println(e.getMessage());
        }


        exit_list_patient();
    }
    public  void no_add_new_ward()
    {
        exit_list_patient();
    }


    public void exit_list_patient()
    {

        btn_back.getScene().getWindow().hide();

    }

}

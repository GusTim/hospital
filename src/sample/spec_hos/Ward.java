package sample.spec_hos;

public class Ward {

    private int id;
    private String name;
    private int max_count;


    public Ward(int id, String name, int max_count)
    {
        this.id = id;
        this.name = name;
        this.max_count = max_count;

    }

    public void setId(int id) {
        this.id = id;
    }
    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getName()
    {
        return name;
    }


    public void setMax_count(int max_count) {
        this.max_count = max_count;
    }
    public int getMax_count()
    {
        return max_count;
    }

}

package sample.spec_hos.change_ward;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.awt.*;
import java.sql.*;

public class Controller_c_w {

    int row_id;

    @FXML
    private TextField edt_ward_name ;
    @FXML
    private TextField edt_ward_count ;
    @FXML
    private TextField edt_ward_id ;

    @FXML
    private Button btn_add;
    @FXML
    private Button btn_back;

    @FXML
    private Button btn_refresh;

    @FXML
    private void initialize() {

        btn_refresh.setOnAction(event -> {
            refresh();
        });

        btn_add.setOnAction(event -> {
            add_new_ward();
        });
        btn_back.setOnAction(event -> {
            no_add_new_ward();
        });

        System.out.println(row_id);

    }

    public void set_change(int row_id)
    {
        this.row_id = row_id;
    }

    public  void add_new_ward()
    {
        int ward_id = Integer.parseInt(edt_ward_id.getText());
        String name = edt_ward_name.getText();
        int ward_count = Integer.parseInt(edt_ward_count.getText());

        String updateTableSQL= "UPDATE  \"wards\" SET \"id\" = '"+ward_id+"' ,\"name\" = '"+ name+"',\"max_count\" = '"+ward_count+"' where \"wards\".\"id\"= "+row_id;


        try {
            Connection dbConnection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","C##GT98","1234");
            Statement statement = dbConnection.createStatement();
            System.out.println(updateTableSQL);
            statement.execute(updateTableSQL);
        } catch (SQLException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Ошибка");
            alert.setHeaderText("Произошла ошибка при изменение!");
            alert.setContentText(e.getMessage());
            alert.showAndWait();
            System.out.println(e.getMessage());
        }


        exit_list_patient();
    }
    public  void no_add_new_ward()
    {
        exit_list_patient();
    }


    public void exit_list_patient()
    {

        btn_back.getScene().getWindow().hide();

    }

    public void refresh()
    {

        String selectTableSQL = "SELECT * from \"wards\" where \"wards\".\"id\"= "+row_id;


        try {
            Connection dbConnection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","C##GT98","1234");
            Statement statement = dbConnection.createStatement();

            // выбираем данные с БД
            ResultSet rs = statement.executeQuery(selectTableSQL);

            // И если что то было получено то цикл while сработает
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int wards_count = rs.getInt("max_count");

                edt_ward_id.setText(String.valueOf(row_id));
                edt_ward_name.setText(name);
                edt_ward_count.setText(String.valueOf(wards_count));

            }
        } catch (SQLException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Ошибка");
            alert.setHeaderText("Произошла ошибка при обновление!");
            alert.setContentText(e.getMessage());
            alert.showAndWait();
            System.out.println(e.getMessage());
        }

    }
}

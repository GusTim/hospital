package sample.spec_hos;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import sample.spec_hos.change_ward.Controller_c_w;

import java.io.IOException;
import java.sql.*;

public class Controller_h {
    private ObservableList<Ward> WardData = FXCollections.observableArrayList();
    private ObservableList<Diadnosis> DiadnosisData = FXCollections.observableArrayList();

    @FXML
    private TableView<Ward> tableWards;

    @FXML
    private TableColumn<Ward, Integer> idColumn_ward;

    @FXML
    private TableColumn<Ward, String> NameColumn_ward;

    @FXML
    private TableColumn<Ward, Integer> DiagnosisColumn_ward;

    @FXML
    private Button btn_add_ward;

    @FXML
    private Button btn_change_ward_1;

    @FXML
    private Button btn_del_ward;

    @FXML
    private Button btn_refrech_ward;

    @FXML
    private Button btn_spec_host;

    @FXML
    private TableView<Diadnosis> tableDiagnosis;

    @FXML
    private TableColumn<Diadnosis, Integer> idColumn_diagnosis;

    @FXML
    private TableColumn<Diadnosis, String> NameColumn_diagnosis;

    @FXML
    private Button btn_add_diagnosis;


    @FXML
    private Button btn_del_diagnosis;

    @FXML
    private Button btn_refresh_diagnosis;

    @FXML
    private Button btn_patient;

    @FXML
    private Button btn_table_wards;




    // инициализируем форму данными
    @FXML
    private void initialize() {
        initData();

        btn_add_ward.setOnAction(event -> {
            add_ward();
        });

        btn_change_ward_1.setOnAction(event -> {
            change_ward();
        });

        btn_del_ward.setOnAction(event -> {
            del_ward();
        });

        btn_refrech_ward.setOnAction(event -> {
            refrech_ward();
        });

        btn_add_diagnosis.setOnAction(event -> {
            add_diagnosis();
        });


        btn_del_diagnosis.setOnAction(event -> {
            del_diagnosis();
        });

        btn_refresh_diagnosis.setOnAction(event -> {
            refresh_diagnosis();
        });

        btn_patient.setOnAction(event -> {
            patient();
        });

        btn_table_wards.setOnAction(event -> {
            table_wards();
        });




        // устанавливаем тип и значение которое должно хранится в колонке
        idColumn_ward.setCellValueFactory(new PropertyValueFactory<Ward, Integer>("id"));
        NameColumn_ward.setCellValueFactory(new PropertyValueFactory<Ward, String>("name"));
        DiagnosisColumn_ward.setCellValueFactory(new PropertyValueFactory<Ward, Integer>("max_count"));

        // заполняем таблицу данными
        tableWards.setItems(WardData);

        // устанавливаем тип и значение которое должно хранится в колонке
        idColumn_diagnosis.setCellValueFactory(new PropertyValueFactory<Diadnosis, Integer>("id"));
        NameColumn_diagnosis.setCellValueFactory(new PropertyValueFactory<Diadnosis, String>("name"));

        // заполняем таблицу данными
        tableDiagnosis.setItems(DiadnosisData);
    }

    // подготавливаем данные для таблицы
    // вы можете получать их с базы данных
    private void initData() {
        update_ward_table();
        update_diagnosis_table();
    }

    public  void update_ward_table()
    {

        tableWards.getItems().clear();

        String selectTableSQL = "SELECT * from \"wards\"";

        try {
            Connection dbConnection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","C##GT98","1234");
            Statement statement = dbConnection.createStatement();

            // выбираем данные с БД
            ResultSet rs = statement.executeQuery(selectTableSQL);

            // И если что то было получено то цикл while сработает
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int max_count = rs.getInt("max_count");

                WardData.add(new Ward(id, name, max_count));

            }
        } catch (SQLException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Ошибка");
            alert.setHeaderText("Произошла ошибка при обновление!");
            alert.setContentText(e.getMessage());
            alert.showAndWait();
            System.out.println(e.getMessage());
        }
    }

    public void change_ward()
    {

        Ward ward = tableWards.getSelectionModel().getSelectedItem();

        int row_id =  ward.getId();


        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/sample/spec_hos/change_ward/change_ward.fxml"));

        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Controller_c_w mainController = loader.<Controller_c_w>getController();
        mainController.set_change(row_id);

        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));

        stage.showAndWait();
    }

    public void del_ward()
    {

        Ward ward = tableWards.getSelectionModel().getSelectedItem();
        System.out.println(ward.getId());

        int row_id =  ward.getId();

        String deleteTableSQL = "DELETE \"wards\" where \"wards\".\"id\" = "+row_id;

        try {
            Connection dbConnection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","C##GT98","1234");
            Statement statement = dbConnection.createStatement();

            // выполняем запрос delete SQL
            statement.execute(deleteTableSQL);
            System.out.println("Record is deleted from table!");
            update_ward_table();
        } catch (SQLException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Ошибка");
            alert.setHeaderText("Произошла ошибка при удаление!");
            alert.setContentText(e.getMessage());
            alert.showAndWait();
            System.out.println(e.getMessage());
        }
    }
    public void add_ward()
    {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/sample/spec_hos/add_ward/add.fxml"));

        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.showAndWait();
    }

    public void refrech_ward()
    {
        update_ward_table();
    }


    public void update_diagnosis_table()
    {
        tableDiagnosis.getItems().clear();

        String selectTableSQL = "SELECT * from \"diagnosis\"";

        try {
            Connection dbConnection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","C##GT98","1234");
            Statement statement = dbConnection.createStatement();

            // выбираем данные с БД
            ResultSet rs = statement.executeQuery(selectTableSQL);

            // И если что то было получено то цикл while сработает
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");

                DiadnosisData.add(new Diadnosis(id, name));

            }
        } catch (SQLException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Ошибка");
            alert.setHeaderText("Произошла ошибка при обновление!");
            alert.setContentText(e.getMessage());
            alert.showAndWait();
            System.out.println(e.getMessage());
        }
    }


    public void del_diagnosis()
       {
        Diadnosis diadnosis = tableDiagnosis.getSelectionModel().getSelectedItem();
        System.out.println(diadnosis.getId());

        int row_id =  diadnosis.getId();

        String deleteTableSQL = "DELETE \"diagnosis\" where \"diagnosis\".\"id\" = "+row_id;

        try {
            Connection dbConnection = DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:XE","C##GT98","1234");
            Statement statement = dbConnection.createStatement();

            // выполняем запрос delete SQL
            statement.execute(deleteTableSQL);
            System.out.println("Record is deleted from table!");
            update_diagnosis_table();
        } catch (SQLException e) {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Ошибка");
            alert.setHeaderText("Произошла ошибка при удаление!");
            alert.setContentText(e.getMessage());
            alert.showAndWait();
            System.out.println(e.getMessage());
        }

    }


    public void refresh_diagnosis()
    {
        update_diagnosis_table();
    }


    public void add_diagnosis()
    {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/sample/spec_hos/add_diagnosis/add.fxml"));

        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.showAndWait();
    }

    public void patient()
    {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/sample/list_patient/sample.fxml"));

        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.showAndWait();

        Stage stage_1 = (Stage) btn_patient.getScene().getWindow();
        // do what you have to do
        stage_1.close();

    }

    public void table_wards()
    {

        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/sample/wards/sample.fxml"));

        try {
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Parent root = loader.getRoot();
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.showAndWait();

        Stage stage_1 = (Stage) btn_patient.getScene().getWindow();
        // do what you have to do
        stage_1.close();

    }












}
